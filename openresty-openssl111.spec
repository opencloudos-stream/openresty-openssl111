%define openssl_prefix          /usr/local/openresty/openssl111
%define zlib_prefix             /usr/local/openresty/zlib
%define openssl_prefix_asan     /usr/local/openresty-asan/openssl111
%define zlib_prefix_asan        /usr/local/openresty-asan/zlib
%define openssl_prefix_debug    /usr/local/openresty-debug/openssl111
%define zlib_prefix_debug       /usr/local/openresty/zlib

Summary:            OpenSSL library for OpenResty
Name:               openresty-openssl111
Version:            1.1.1w
Release:            6%{?dist}
License:            OpenSSL
URL:                https://openresty.org/
Source0:            https://www.openssl.org/source/openssl-%{version}.tar.gz

Patch0001:          openssl-1.1.1f-sess_set_get_cb_yield.patch

Patch3000:          copy-dir.patch


BuildRequires:      gcc, make, perl
BuildRequires:      openresty-zlib-devel >= 1.2.11
Requires:           openresty-zlib >= 1.2.11

AutoReqProv:        no

%description
This OpenSSL library build is specifically for OpenResty uses. It may contain
custom patches from OpenResty.


%package devel
Summary:            Development files for OpenResty's OpenSSL library
Requires:           %{name} = %{version}-%{release}

%description devel
Provides C header and static library for OpenResty's OpenSSL library.

%package asan
Summary:            Clang AddressSanitizer Debug version of the OpenSSL library for OpenResty
BuildRequires:      ccache, gcc, make, perl, clang, compiler-rt, libasan
BuildRequires:      openresty-zlib-asan-devel >= 1.2.11-6
Requires:           openresty-zlib-asan >= 1.2.11-6

AutoReqProv:        no

%description asan
This is the clang AddressSanitizer version of the OpenSSL library build for OpenResty uses.

%package asan-devel
Summary:            Clang AddressSanitizer version of development files for OpenResty's OpenSSL library
Requires:           openresty-openssl111-asan = %{version}-%{release}

%description asan-devel
Provides C header and static library for the clang AddressSanitizer version of OpenResty's OpenSSL library. This is the clang AddressSanitizer version.

%package debug 
Summary:            Debug version of the OpenSSL library for OpenResty
Requires:           openresty-zlib >= 1.2.11

AutoReqProv:        no

%description debug
This is the debug version of the OpenSSL library build for OpenResty uses.

%package debug-devel
Summary:            Debug version of development files for OpenResty's OpenSSL library
Requires:           openresty-openssl111-debug = %{version}-%{release}

%description debug-devel
Provides C header and static library for the debug version of OpenResty's OpenSSL library. This is the debug version.

%prep
%autosetup -p1 -n openssl-%{version}



%build
bash ./copy-dir.sh
./config \
    shared zlib -g3 \
    enable-camellia enable-seed enable-rfc3779 \
    enable-cms enable-md2 enable-rc5 \
    enable-weak-ssl-ciphers \
    enable-ssl3 enable-ssl3-method \
    --prefix=%{openssl_prefix} \
    --libdir=lib \
    -I%{zlib_prefix}/include \
    -L%{zlib_prefix}/lib \
    -Wl,-rpath,%{zlib_prefix}/lib:%{openssl_prefix}/lib

ncpus=`nproc`
if [ "$ncpus" -gt 16 ]; then
    ncpus=16
fi
make CC='ccache gcc -fdiagnostics-color=always' -j$ncpus

cd asan
export ASAN_OPTIONS=detect_leaks=0

./config \
    no-asm \
    enable-camellia enable-seed enable-rfc3779 \
    enable-cms enable-md2 enable-rc5 \
    enable-weak-ssl-ciphers \
    enable-ssl3 enable-ssl3-method \
    shared zlib -g3 -O1 -DPURIFY \
    --prefix=%{openssl_prefix_asan} \
    --libdir=lib \
    -I%{zlib_prefix_asan}/include \
    -L%{zlib_prefix_asan}/lib \
    -Wl,-rpath,%{zlib_prefix_asan}/lib:%{openssl_prefix_asan}/lib

ncpus=`nproc`
if [ "$ncpus" -gt 16 ]; then
    ncpus=16
fi
make -j$ncpus \
    LD_LIBRARY_PATH= \
    CC="ccache gcc -fsanitize=address" \
    > /dev/stderr
cd -

cd debug
./config \
    no-asm \
    enable-camellia enable-seed enable-rfc3779 \
    enable-cms enable-md2 enable-rc5 \
    enable-weak-ssl-ciphers \
    enable-ssl3 enable-ssl3-method \
    shared zlib -g3 -O0 -DPURIFY \
    --prefix=%{openssl_prefix_debug} \
    --libdir=lib \
    -I%{zlib_prefix_debug}/include \
    -L%{zlib_prefix_debug}/lib \
    -Wl,-rpath,%{zlib_prefix_debug}/lib:%{openssl_prefix_debug}/lib

sed -i 's/ -O3 / -O0 /g' Makefile

make CC='ccache gcc -fdiagnostics-color=always' -j`nproc`
cd -


%install
make install_sw DESTDIR=%{buildroot}

chmod 0755 %{buildroot}%{openssl_prefix}/lib/*.so*
chmod 0755 %{buildroot}%{openssl_prefix}/lib/*/*.so*

rm -rf %{buildroot}%{openssl_prefix}/bin/c_rehash
rm -rf %{buildroot}%{openssl_prefix}/lib/pkgconfig
rm -rf %{buildroot}%{openssl_prefix}/misc

cd asan 
make install_sw DESTDIR=%{buildroot}

chmod +w %{buildroot}%{openssl_prefix_asan}/lib/*.so
chmod +w %{buildroot}%{openssl_prefix_asan}/lib/*/*.so

rm -rf %{buildroot}%{openssl_prefix_asan}/bin/c_rehash
rm -rf %{buildroot}%{openssl_prefix_asan}/lib/pkgconfig
rm -rf %{buildroot}%{openssl_prefix_asan}/misc
cd -

cd debug 
make install_sw DESTDIR=%{buildroot}

chmod +w %{buildroot}%{openssl_prefix_debug}/lib/*.so
chmod +w %{buildroot}%{openssl_prefix_debug}/lib/*/*.so

rm -rf %{buildroot}%{openssl_prefix_debug}/bin/c_rehash
rm -rf %{buildroot}%{openssl_prefix_debug}/lib/pkgconfig
rm -rf %{buildroot}%{openssl_prefix_debug}/misc
cd -

export QA_RPATHS=$[ 0x0002 ]

%files
%defattr(-,root,root,-)

%dir %{openssl_prefix}
%dir %{openssl_prefix}/bin
%dir %{openssl_prefix}/lib
%attr(0755,root,root) %{openssl_prefix}/bin/openssl
%attr(0755,root,root) %{openssl_prefix}/lib/*.so*
%attr(0755,root,root) %{openssl_prefix}/lib/*/*.so*


%files devel
%defattr(-,root,root,-)

%dir %{openssl_prefix}/include
%{openssl_prefix}/include/*
%{openssl_prefix}/lib/*.a

%files asan
%defattr(-,root,root,-)

%attr(0755,root,root) %{openssl_prefix_asan}/bin/openssl
%attr(0755,root,root) %{openssl_prefix_asan}/lib/*.so*
%attr(0755,root,root) %{openssl_prefix_asan}/lib/*/*.so*


%files asan-devel
%defattr(-,root,root,-)

%{openssl_prefix_asan}/include/*
%attr(0755,root,root) %{openssl_prefix_asan}/lib/*.a

%files debug
%defattr(-,root,root,-)

%attr(0755,root,root) %{openssl_prefix_debug}/bin/openssl
%attr(0755,root,root) %{openssl_prefix_debug}/lib/*.so*
%attr(0755,root,root) %{openssl_prefix_debug}/lib/*/*.so*


%files debug-devel
%defattr(-,root,root,-)

%{openssl_prefix_debug}/include/*
%attr(0755,root,root) %{openssl_prefix_debug}/lib/*.a

%changelog
* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.1w-6
- Rebuilt for loongarch release

* Tue Apr 16 2024 Feng Weiyao <wynnfeng@tencent.com> - 1.1.1w-5
- set AutoReqProv no for debug

* Wed Jan 31 2024 Feng Weiyao <wynnfeng@tencent.com> - 1.1.1w-4
- set AutoReqProv no

* Mon Dec 25 2023 luffyluo <luffyluo@tencent.com> - 1.1.1w-3
- Rebuilt for llvm 17.0.6

* Fri Dec 8 2023 Feng Weiyao <wynnfeng@tencent.com> - 1.1.1w-2
- initial build for opencloudos

* Mon Nov 27 2023 Yichun Zhang (agentzh) 1.1.1w-1
- upgraded OpenSSL to 1.1.1w.

* Mon Apr 17 2023 Yichun Zhang (agentzh) 1.1.1t-1
- upgraded OpenSSL to 1.1.1t.

* Thu Nov 3 2022 Yichun Zhang (agentzh) 1.1.1s-1
- upgraded OpenSSL to 1.1.1s.

* Fri Mar 18 2022 Yichun Zhang (agentzh) 1.1.1n-1
- upgraded OpenSSL to 1.1.1n.

* Tue May 11 2021 Jiahao Wang 1.1.1k-1
- upgraded OpenSSL to 1.1.1k.

* Thu Dec 10 2020 Yichun Zhang (agentzh) 1.1.1i-1
- upgraded OpenSSL to 1.1.1i.

* Mon May 14 2018 Yichun Zhang (agentzh) 1.1.0h-1
- upgraded openresty-openssl to 1.1.0h.

* Thu Apr 19 2018  Yichun Zhang (agentzh) 1.0.2n-1
- upgraded openssl to 1.0.2n.

* Sun Mar 19 2017 Yichun Zhang (agentzh)
- upgraded OpenSSL to 1.0.2k.

* Fri Nov 25 2016 Yichun Zhang (agentzh)
- added perl to the BuildRequires list.

* Tue Oct  4 2016 Yichun Zhang (agentzh)
- fixed the rpath of libssl.so (we should have linked against
our own libcrypto.so).

* Sat Sep 24 2016 Yichun Zhang (agentzh)
- upgrade to OpenSSL 1.0.2i.

* Tue Aug 23 2016 zxcvbn4038 1.0.2k
- use openresty-zlib instead of the system one.

* Wed Jul 13 2016 makerpm 1.0.2h
- initial build for OpenSSL 1.0.2h.